#! /bin/bash -xe
#  st2mistral pkgdeps
apt-get -qq update 

#Get Curl
apt-get -qq install -y \
  software-properties-common \
  curl

#Add ST2 Repo
curl -s https://packagecloud.io/install/repositories/StackStorm/staging-stable/script.deb.sh | bash && \

#Install apt-get deps
apt-get -qq update
apt-get -qq install -y  \
  git \
  libffi-dev \
  libssl-dev \
  libxml2-dev \
  libxslt1-dev \
  libyaml-dev \
  mc \
  libpq-dev \
  python-dev \
  python-pip \
  python-setuptools \
  st2mistral=$ST2_MISTRAL_VERSION
rm -rf /var/lib/apt/lists/* 

#Install Python Deps
pip install tox==1.6.1 python-mistralclient
pip install -r /tmp/requirements.txt

